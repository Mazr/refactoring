package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 * 
 */
public class OrderReceipt {
    public static final double TAX_RATE = .10;
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
	}

	public String printReceipt() {
		StringBuilder output = new StringBuilder();
        printHeader(output);
		double totalSalesTax = 0d;
		double total = 0d;
		StringBuilder finalOutput = output;
		for (LineItem lineItem : order.getLineItems()) {
			formatString(output, lineItem);
            totalSalesTax += getSalesTax(lineItem);
            total += (lineItem.totalAmount() + getSalesTax(lineItem));
		}
		printSalesTax(output, totalSalesTax, "Sales Tax");
		printSalesTax(output, total, "Total Amount");
		return output.toString();
	}

    private void printHeader(StringBuilder output) {
        String printTitle = "======Printing Orders======\n";
        output.append(printTitle);
        output.append(order.getCustomerName());
        output.append(order.getCustomerAddress());
    }

    private StringBuilder printSalesTax(StringBuilder output, double totalSalesTax, String s) {
        return output.append(s).append('\t').append(totalSalesTax);
    }

    private double getSalesTax(LineItem lineItem) {
        double salesTax = lineItem.totalAmount() * TAX_RATE;
        return salesTax;

    }

    private void formatString(StringBuilder output, LineItem lineItem) {
		output.append(lineItem.getDescription());
		output.append('\t');
		output.append(lineItem.getPrice());
		output.append('\t');
		output.append(lineItem.getQuantity());
		output.append('\t');
		output.append(lineItem.totalAmount());
		output.append('\n');
	}
}